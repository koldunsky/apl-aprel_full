<?php

$email 	= $_POST['email'];
//$email 	= "rustamov-ra@ya.ru";

if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
	echo 'Неверный адрес Email';
	return;
}

$name 	= $_POST['name'] != "null null" ? $_POST['name'] : "";
$img 	= $_POST['img'];
$date 	= $_POST['date'];

$final_digit = $_POST['final_digit'];
$man_digit = $_POST['man_digit'];
$woman_digit = $_POST['woman_digit'];

$matrix	= $_POST['matrix'];
$matrix_format = [];
$intersections = $_POST['intersections'];
$filename = "aprel_app_charts/".$date.".jpg";


$matrix_format[1] = $matrix[1];
$matrix_format[2] = $matrix[4];
$matrix_format[3] = $matrix[7];
$matrix_format[4] = $matrix[2];
$matrix_format[5] = $matrix[5];
$matrix_format[6] = $matrix[8];
$matrix_format[7] = $matrix[3];
$matrix_format[8] = $matrix[6];
$matrix_format[9] = $matrix[9];


$subject = "Расчет теста. ".$name." ".$date;

$headers = "From: app@apl-aprel.ru\r\n";
$headers .= "Reply-To: app@apl-aprel.ru\r\n";
$headers .= "MIME-Version: 1.0\r\n";
$headers .= "Content-Type: text/html; charset=UTF-8\r\n";

$message = '<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta charset="utf-8"> <!-- utf-8 works for most cases -->
	<meta name="viewport" content="width=device-width"> <!-- Forcing initial-scale shouldn\'t be necessary -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge"> <!-- Use the latest (edge) version of IE rendering engine -->
    <meta name="x-apple-disable-message-reformatting">  <!-- Disable auto-scale in iOS 10 Mail entirely -->
	<title>'.$subject.'</title> <!-- The title tag shows in email notifications, like Android 4.4. -->

	<!-- Web Font / @font-face : BEGIN -->
	<!-- NOTE: If web fonts are not required, lines 10 - 27 can be safely removed. -->
	
	<!-- Desktop Outlook chokes on web font references and defaults to Times New Roman, so we force a safe fallback font. -->
	<!--[if mso]>
		<style>
			* {
				font-family: sans-serif !important;
			}
		</style>
	<![endif]-->
	
	<!-- All other clients get the webfont reference; some will render the font and others will silently fail to the fallbacks. More on that here: http://stylecampaign.com/blog/2015/02/webfont-support-in-email/ -->
	<!--[if !mso]><!-->
		<!-- insert web font reference, eg: <link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet" type="text/css"> -->
	<!--<![endif]-->

	<!-- Web Font / @font-face : END -->
	
	<!-- CSS Reset -->
    <style>

		/* What it does: Remove spaces around the email design added by some email clients. */
		/* Beware: It can remove the padding / margin and add a background color to the compose a reply window. */
        html,
        body {
	        margin: 0 auto !important;
            padding: 0 !important;
            height: 100% !important;
            width: 100% !important;
        }
        
        /* What it does: Stops email clients resizing small text. */
        * {
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%;
        }
        
        /* What is does: Centers email on Android 4.4 */
        div[style*="margin: 16px 0"] {
            margin:0 !important;
        }
        
        /* What it does: Stops Outlook from adding extra spacing to tables. */
        table,
        td {
            mso-table-lspace: 0pt !important;
            mso-table-rspace: 0pt !important;
        }
                
        /* What it does: Fixes webkit padding issue. Fix for Yahoo mail table alignment bug. Applies table-layout to the first 2 tables then removes for anything nested deeper. */
        table {
            border-spacing: 0 !important;
            border-collapse: collapse !important;
            table-layout: fixed !important;
            margin: 0 auto !important;
        }
        table table table {
            table-layout: auto; 
        }
        
        /* What it does: Uses a better rendering method when resizing images in IE. */
        img {
            -ms-interpolation-mode:bicubic;
        }
        
        /* What it does: A work-around for iOS meddling in triggered links. */
        .mobile-link--footer a,
        a[x-apple-data-detectors] {
            color:inherit !important;
            text-decoration: underline !important;
        }

        /* What it does: Prevents underlining the button text in Windows 10 */
        .button-link {
            text-decoration: none !important;
        }
      
    </style>
    
    <!-- Progressive Enhancements -->
    <style>
        
        /* What it does: Hover styles for buttons */
        .button-td,
        .button-a {
            transition: all 100ms ease-in;
        }
        .button-td:hover,
        .button-a:hover {
            background: #ed7a61 !important;
            border-color: #ed7a61 !important;
        }

        /* Media Queries */
        @media screen and (max-width: 600px) {

            .email-container {
                width: 100% !important;
                margin: auto !important;
            }

            /* What it does: Forces elements to resize to the full width of their container. Useful for resizing images beyond their max-width. */
            .fluid {
                max-width: 100% !important;
                height: auto !important;
                margin-left: auto !important;
                margin-right: auto !important;
            }

            /* What it does: Forces table cells into full-width rows. */
            .stack-column,
            .stack-column-center {
                display: block !important;
                width: 100% !important;
                max-width: 100% !important;
                direction: ltr !important;
            }
            /* And center justify these ones. */
            .stack-column-center {
                text-align: center !important;
            }
        
            /* What it does: Generic utility class for centering. Useful for images, buttons, and nested tables. */
            .center-on-narrow {
                text-align: center !important;
                display: block !important;
                margin-left: auto !important;
                margin-right: auto !important;
                float: none !important;
            }
            table.center-on-narrow {
                display: inline-block !important;
            }
                
        }

    </style>

</head>';

$message .= '<body bgcolor="#EEEEEE" width="100%" style="margin: 0;">
    <center style="width: 100%; background: #f1f1f1;">

        <!-- Visually Hidden Preheader Text : BEGIN -->
        <div style="display:none;font-size:1px;line-height:1px;max-height:0px;max-width:0px;opacity:0;overflow:hidden;mso-hide:all;font-family: sans-serif;">
            Расчет теста по дате рождения '.$date.'г.
        </div>
        <!-- Visually Hidden Preheader Text : END -->';
		
$message .= '<!-- Email Header : BEGIN -->
        <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="600" style="margin: auto;" class="email-container">
			<tr>
				<td bgcolor="#FFFFFF" style="padding: 20px 0; text-align: center">
					<img src="http://apl-aprel.ru/wp-content/themes/aprel/static/img/aprel_email_logo.png" width="250" height="100" alt="alt_text" border="0" style="height: auto; background: #dddddd; font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555;">
				</td>
			</tr>
        </table>
        <!-- Email Header : END -->';
		
		
$message .= '        <!-- Email Body : BEGIN -->
        <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="600" style="margin: auto;" class="email-container">

            <!-- 1 Column Text : BEGIN -->
            <tr>
                <td bgcolor="#ffffff" style="padding: 0 40px; text-align: center; font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555;">';
$message .= '		<h2 style="line-height: 22px; font-size: 18px; color: #555555; background: #fff">Расчет теста по дате рождения</h2>';

if(strlen($name)) {	
	$message .= '	<h1 style="line-height: 36px; font-size: 30px; color: #111; background: #fff">';
	$message .= 	$name;
	$message .= '	</h1>';
}

$message .= '		<h2 style="line-height: 22px; font-size: 18px; color: #111; background: #fff">';
$message .= 		$date;
$message .= '		</h2>';

$message .= '	</td>
            </tr>
            <!-- 1 Column Text : END -->';
			
$message .= '<!-- Chart : BEGIN -->
            <tr>
                <td bgcolor="#ffffff" align="center" valign="top" style="padding: 0 10px;">';	
										
$message .=	'		<img src="http://apl-aprel.ru/aprel_app_charts/chart_legend.png" alt="Легенда для графика: зеленый - судьба; желтый - воля; оранжевый - пересечения; голубой - время проведения теста" width="580" height="57" border="0" class="fluid" style="height: auto; background: #dddddd; font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555;">';						
$message .=	'		<img src="http://apl-aprel.ru/'. $filename .'" alt="chart for date '.$date.'" width="580" height="580" border="0" class="fluid" style="height: auto; background: #dddddd; font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555;">';

$message .= '	</td>
            </tr>
            <!-- Chart : END -->';

$message .= '<!-- 3 Even Columns : BEGIN -->
            <tr>
                <td bgcolor="#ffffff" align="center" valign="top" style="padding: 10px;">
                    <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                        <tr>
                            <td>
								<h2 style="margin-top: 35px; margin-bottom: 20px; line-height: 26px; font-size: 22px; color: #555; background: #FFFFFF; text-align: center;">Точки пересечения</h2>
								
                                <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                                    <tr>
                                        <td style="width: 50%; font-family: sans-serif; font-size: 18px; mso-height-rule: exactly; line-height: 20px; color: #AAAAAA; padding: 10px; text-align: center; border-right: 2px solid #ddd; border-bottom: 2px solid #ddd;">Дата</td>
										<td style="width: 50%; font-family: sans-serif; font-size: 18px; mso-height-rule: exactly; line-height: 20px; color: #AAAAAA; padding: 10px;  text-align: center; border-bottom: 2px solid #ddd;">Возраст</td>
                                    </tr>';
					for($i = 0; $i < count($intersections); $i++) {
						$intersection = explode("-", $intersections[$i]);
						$message .= '<tr>
										<td style="width: 50%; font-family: sans-serif; font-size: 18px; mso-height-rule: exactly; line-height: 20px; color: #555555; padding: 10px; text-align: center;  border-right: 2px solid #ddd;">';
						$message .= $intersection[0];
						$message .= '	</td>
										<td style="width: 50%; font-family: sans-serif; font-size: 18px; mso-height-rule: exactly; line-height: 20px; color: #E85433; padding: 10px; text-align: center;">';
						$message .= $intersection[1];
						$message .= '	</td>
									</tr>';
					}
									
$message .= 					'</table>
                            </td>                            
                        </tr>
                    </table>
                </td>
            </tr>
            <!-- 3 Even Columns : END -->';

$message .= '<!-- 2 Even Columns : BEGIN -->
            <tr>
                <td bgcolor="#ffffff" align="center" valign="top" style="padding: 10px;">
					<h2 style="margin-top: 35px; margin-bottom: 20px; line-height: 26px; font-size: 22px; color: #555; background: #FFFFFF; text-align: center;">Матрица и цифры личности</h2>
                    <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                        <tr>';
$message .= '
                            <!-- Column : BEGIN -->
							<!-- Matrix -->
                            <td class="stack-column-center"  style="width: 46%; padding: 0 2%;">
                                <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="border-collapse: collapse;    border-style: hidden;">
                                    <tr>';
									for($i = 1; $i <= 9; $i++) {
										$message .= '<td style="font-family: sans-serif; font-size: 18px; mso-height-rule: exactly; line-height: 20px; color: #555555; padding: 20px 10px; text-align: center; border: 2px solid #ddd; border-bottom: none;"width="33.33333%">';
										$message .= $matrix_format[$i] ? $matrix_format[$i] : "-";
										$message .= '</td>';
										
										if($i % 3 == 0) {
											$message .= '</tr><tr>';
										}
									}

$message .= '						</tr>
                                </table>
                            </td>
                            <!-- Column : END -->';
						
$message .= '				<!-- Column : BEGIN -->
                            <td class="stack-column-center" style="width: 46%; padding: 0 2%;">
								<table role="presentation" cellspacing="0" cellpadding="0" border="0" style="width: 100%">
									<tr>
										<td style="font-family: sans-serif; font-size: 18px; mso-height-rule: exactly; line-height: 20px; color: #555555; padding: 10px; border-bottom: 10px solid #ffffff; border-top: 10px solid #ffffff; background: #f1f1f1; width: 70%; text-align: left;">
											Итоговая цифра
										</td>
										<td style="font-family: sans-serif; font-size: 18px; mso-height-rule: exactly; line-height: 20px; color: #E85433; padding: 10px; border-bottom: 10px solid #ffffff; border-top: 10px solid #ffffff; background: #f1f1f1;  width: 30%; text-align: right;">'.
										$final_digit
										.'</td>
									</tr>
									<tr>
										<td style="font-family: sans-serif; font-size: 18px; mso-height-rule: exactly; line-height: 20px; color: #555555; padding: 10px; border-bottom: 10px solid #ffffff; border-top: 10px solid #ffffff; background: #f1f1f1; width: 70%; text-align: left;">
											Мужские цифры
										</td>
										<td style="font-family: sans-serif; font-size: 18px; mso-height-rule: exactly; line-height: 20px; color: #E85433; padding: 10px; border-bottom: 10px solid #ffffff; border-top: 10px solid #ffffff; background: #f1f1f1;  width: 30%; text-align: right">'.
										$man_digit
										.'</td>
									</tr>
									<tr>
										<td style="font-family: sans-serif; font-size: 18px; mso-height-rule: exactly; line-height: 20px; color: #555555; padding: 10px; border-bottom: 10px solid #ffffff; border-top: 10px solid #ffffff; background: #f1f1f1; width: 70%; text-align: left;">
											Женские цифры
										</td>
										<td style="font-family: sans-serif; font-size: 18px; mso-height-rule: exactly; line-height: 20px; color: #E85433; padding: 10px; border-bottom: 10px solid #ffffff; border-top: 10px solid #ffffff; background: #f1f1f1;  width: 30%; text-align: right">'.
										$woman_digit
										.'</td>
									</tr>
								</table>
							</td>
                            <!-- Column : END -->';
										
$message .= '			</tr>
                    </table>
                </td>
            </tr>
            <!-- 2 Even Columns : END -->';		
			

			
$message .= '<!-- Clear Spacer : BEGIN -->
            <tr>
                <td height="40" style="font-size: 0; line-height: 0;">
                    &nbsp;
                </td>
            </tr>
            <!-- Clear Spacer : END -->';

$message .= '<!-- 1 Column Text + Button : BEGIN -->
            <tr>
                <td bgcolor="#ffffff">
                    <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                    	<tr>
                            <td style="padding: 40px; font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555;">
                                Расчет подготовлен приложением &laquo;<a href="https://play.google.com/store/apps/details?id=ru.apl_aprel.aprel&hl=ru" target="_blank" title="Страница приложения в Google Play">Академия Апрель</a>&raquo; для android.
                            </td>
							</tr>
                    </table>
                </td>
            </tr>
            <!-- 1 Column Text + Button : BEGIN -->';
			
$message .= '
        </table>
        <!-- Email Body : END -->';
		
$message .= '<!-- Email Footer : BEGIN -->
        <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="600" style="margin: auto;" class="email-container">
            <tr>
                <td style="padding: 40px 10px;width: 100%;font-size: 12px; font-family: sans-serif; mso-height-rule: exactly; line-height:18px; text-align: center; color: #888888;">
                    <br><br>
                    Академия преобразования личности &laquo;Апрель&raquo;<br><br><span class="mobile-link--footer">109377, Москва, Рязанский пр-т, д. 34</span><br><br><span class="mobile-link--footer">+7 965 322-76-84<br>+7 905 543-11-45</span>
                    <br><br>
                </td>
            </tr>
        </table>
        <!-- Email Footer : END -->';
$message .= '
    </center>
</body>
</html>';

function base64_to_jpeg( $base64_string, $output_file ) {
    $ifp = fopen( $output_file, "wb" ); 
    fwrite( $ifp, base64_decode( $base64_string) ); 
    fclose( $ifp ); 
    return( $output_file ); 
}

base64_to_jpeg($img, $filename);

// Send email!
if(mail($email, $subject, $message, $headers)) 
//if(1) 
{
	echo "Письмо на адрес ". $email ." отправлено!";
} else {
	echo "Что-то пошло не так, попробуйте позднее";
}
?>