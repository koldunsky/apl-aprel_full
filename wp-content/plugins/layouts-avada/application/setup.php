<?php

/**
 * Singleton for setting up the integration.
 *
 * Note that it doesn't have to have unique name. Because of autoloading, it will be loaded only once (when this
 * integration plugin is operational).
 */
/** @noinspection PhpUndefinedClassInspection */
class WPDDL_Integration_Setup extends WPDDL_Theme_Integration_Setup_Abstract {
	public $zindexs;
	public $zidx;

	/**
	 * Run Integration.
	 *
	 * @return bool|WP_Error True when the integration was successful or a WP_Error with a sensible message
	 *     (which can be displayed to the user directly).
	 */
	public function run() {
		$this->zindexs = array('idx-a', 'idx-b');
		$this->zidx = 0;

		// Load default layouts
		$this->set_layouts_path( dirname( dirname( __FILE__) ) . DIRECTORY_SEPARATOR . 'public/layouts' );

		parent::run();
		return true;
	}


	/**
	 * @return string
	 */
	protected function get_supported_theme_version() {
		return '4.0';
	}

	/**
	 * Build URL of an resource from path relative to plugin's root directory.
	 *
	 * @param string $relative_path Some path relative to the plugin's root directory.
	 * @return string URL of the given path.
	 */
	protected function get_plugins_url( $relative_path ) {
		return plugins_url( '/../' . $relative_path , __FILE__ );
	}

	/**
	 * Get list of templates supported by Layouts with this theme.
	 *
	 * @return array Associative array with template file names as keys and theme names as values.
	 */
	protected function get_supported_templates() {
		return array(
				$this->getPageDefaultTemplate() => __( 'Template page', 'ddl-layouts' )
		);
	}


	/**
	 * Layouts Support
	 *
	 * Implement theme-specific logic here. For example, you may want to:
	 *     - if theme has it's own loop, replace it by the_ddlayout()
	 *     - remove headers, footer, sidebars, menus and such, if achievable by filters
	 *     - otherwise you will have to resort to something like redirecting templates (see the template router below)
	 *     - add $this->clear_content() to some filters to remove unwanted site structure elements
	 */
	protected function add_layouts_support() {
		parent::add_layouts_support();

		/** @noinspection PhpUndefinedClassInspection */
		WPDDL_Integration_Theme_Template_Router::get_instance();

		add_filter('ddl-get_container_class', array(&$this, 'add_fusion_row'), 10, 1);
		add_filter('ddl-get_container_class', array(&$this, 'disable_logo_resize'), 11, 1);
		add_filter('ddl-get_container_fluid_class', array(&$this, 'disable_logo_resize'), 11, 1);
	}

	function add_fusion_row( $el ){
			return "{$el} fusion-row";
	}

	/**
	 * Add custom theme elements to Layouts.
	 */
	protected function add_layouts_cells() {
		// Header Cell
		$header = new WPDDL_Integration_Layouts_Cell_Header();
		$header->setup();

		// Menu Cell
		$menu = new WPDDL_Integration_Layouts_Cell_Menu();
		$menu->setup();

		// Secondary Menu
		$secondary_menu = new WPDDL_Integration_Layouts_Cell_Secondary_Menu();
		$secondary_menu->setup();

		// Title Bar Cell
		$title_bar = new WPDDL_Integration_Layouts_Cell_Title_Bar();
		$title_bar->setup();

		// Sidebar Cell
		$sidebar = new WPDDL_Integration_Layouts_Cell_Sidebar();
		$sidebar->setup();

		// Footer Cell
		$footer = new WPDDL_Integration_Layouts_Cell_Footer();
		$footer->setup();

		// Logo Cell
 		$logo = new WPDDL_Integration_Layouts_Cell_Logo();
		$logo->setup();

		// Search Cell
		$search = new WPDDL_Integration_Layouts_Cell_Search();
		$search->setup();

		// Social Icons
		$social_icons = new WPDDL_Integration_Layouts_Cell_Social_Icons();
		$social_icons->setup();

		// Contact Info
		$contact_info = new WPDDL_Integration_Layouts_Cell_Contact_Info();
		$contact_info->setup();


	}

	public function moveCellCategoryToTop( $categories ) {
		$temp = array( 'Avada' => $categories['Avada'] );
		unset( $categories['Avada'] );
		return  $temp + $categories;
	}

	/**
	 * This function is used to remove all theme settings which are obsolete with the use of Layouts
	 * i.e. "Default Layout" in "Theme Settings"
	 */
	protected function modify_theme_settings() {
		// ...
	}

	public function disable_logo_resize( $el ) {
		return "{$el} avada-logo-integration";
	}
}
