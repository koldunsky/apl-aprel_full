<?php
$min_enableBuilder = false;
$min_builderPassword = 'admin';
$min_errorLogger = true;
$min_allowDebugFlag = true;
$min_cachePath = dirname(dirname(__FILE__)) . '/cache';
$min_documentRoot = '';
$min_cacheFileLocking = false;
$min_serveOptions['bubbleCssImports'] = false;
$min_serveOptions['maxAge'] = 604800;
$min_serveOptions['minApp']['groupsOnly'] = false;
$min_symlinks = array();
$min_uploaderHoursBehind = 0;
$min_libPath = dirname(__FILE__) . '/lib';
ini_set('zlib.output_compression', '0');
// auto-generated on 2016-12-24 17:42:15
