<?php

/**
 * Hooks into the template_include filter and select different page template for content that has an Layout assigned.
 */
final class WPDDL_Integration_Theme_Template_Router {


	private static $instance = null;


	public static function get_instance() {
		if( null == self::$instance ) {
			self::$instance = new self;
		}
		return self::$instance;
	}


	private function __construct() {
		add_filter( 'template_include', array( $this, 'template_include' ) );
	}


	public function template_include( $template ) {

		if( is_ddlayout_assigned() ) {
			$template_file = 'template-default.php';

			if( null != $template_file ) {
				$template = dirname( __FILE__ ) . '/view/' . $template_file;
			}

			// modify body class
			add_filter( 'body_class', array( $this, 'modifyBodyClass' ) );
		}

		return $template;
	}

	function modifyBodyClass( $body_class ) {
		$body_class[] = 'layouts-active';

		return $body_class;
	}
}