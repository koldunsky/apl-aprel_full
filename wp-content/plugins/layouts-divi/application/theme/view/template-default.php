<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages that use a Layout.
 */

include 'header.php';

the_ddlayout();

include 'footer.php';
