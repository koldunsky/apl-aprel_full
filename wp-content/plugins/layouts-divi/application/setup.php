<?php
/**
 * Singleton for setting up the integration.
 *
 * Note that it doesn't have to have unique name. Because of autoloading, it will be loaded only once (when this
 * integration plugin is operational).
 *
 */
/** @noinspection PhpUndefinedClassInspection */
class WPDDL_Integration_Setup extends WPDDL_Theme_Integration_Setup_Abstract {
	protected $custom_frontend_js_path;

	/**
	 * Run Integration.
	 *
	 * @return bool|WP_Error True when the integration was successful or a WP_Error with a sensible message
	 *     (which can be displayed to the user directly).
	 */
	public function run() {
		$this->custom_frontend_js_path = 'public/js/custom-frontend.js';

	    // reorder cell categories
	    add_filter( 'ddl-get_cell_categories', array( $this, 'moveCellCategoryToTop' ) );

		// Load default layouts
		$this->set_layouts_path( dirname( dirname( __FILE__) ) . DIRECTORY_SEPARATOR . 'public/layouts' );

	    parent::run();
		return true;
	}

	/*public function add_bootstrap_support() {
		// Remove bootstrap support, because Divi contains her own definitions.
	}*/

	public function frontend_enqueue() {
		parent::frontend_enqueue();

		if( is_ddlayout_assigned() ) {
			wp_register_script(
					'layouts-theme-integration-frontend-js',
					$this->get_plugins_url( $this->custom_frontend_js_path ),
					array(),
					$this->get_supported_theme_version()
			);

			wp_enqueue_script( 'layouts-theme-integration-frontend-js' );
		}
	}

	public function admin_enqueue() {
		parent::admin_enqueue();
        wp_enqueue_script( 'layouts-theme-integration-backend' );
	}

    function get_custom_backend_js_path(){
        return 'public/js/theme-integration-admin.js';
    }


	/**
	 * @return string
	 */
	protected function get_supported_theme_version() {
		return '2.6.1';
	}


	/**
	 * Build URL of an resource from path relative to plugin's root directory.
	 *
	 * @param string $relative_path Some path relative to the plugin's root directory.
	 * @return string URL of the given path.
	 */
	protected function get_plugins_url( $relative_path ) {
		return plugins_url( '/../' . $relative_path , __FILE__ );
	}


	/**
	 * Get list of templates supported by Layouts with this theme.
	 *
	 * @return array Associative array with template file names as keys and theme names as values.
	 */
	protected function get_supported_templates() {
		return array(
			$this->getPageDefaultTemplate() => __( 'Template page', 'ddl-layouts' )
		);
	}


	/**
	 * Layouts Support
	 *
	 * Implement theme-specific logic here. For example, you may want to:
	 *     - if theme has it's own loop, replace it by the_ddlayout()
	 *     - remove headers, footer, sidebars, menus and such, if achievable by filters
	 *     - otherwise you will have to resort to something like redirecting templates (see the template router below)
	 *     - add $this->clear_content() to some filters to remove unwanted site structure elements
	 */
	protected function add_layouts_support() {

	    parent::add_layouts_support();

	    /** @noinspection PhpUndefinedClassInspection */
	    WPDDL_Integration_Theme_Template_Router::get_instance();

		//add_filter('ddl-get_container_class', array(&$this, 'disable_default_container'), 10, 1);
	}

	function disable_default_container($el) {
		return '';
	}

	/**
	 * Add custom theme elements to Layouts.
	 *
	 */
	protected function add_layouts_cells() {

	    // logo
	    $logo = new WPDDL_Integration_Layouts_Cell_Logo();
	    $logo->setup();

	    // primary nav
	    $primary_navigation = new WPDDL_Integration_Layouts_Cell_Primary_Navigation();
	    $primary_navigation->setup();

	    // social icons
	    $social_icons = new WPDDL_Integration_Layouts_Cell_Social_Icons();
	    $social_icons->setup();

	    // sidebar
	    $sidebar = new WPDDL_Integration_Layouts_Cell_Sidebar();
	    $sidebar->setup();

	}
	
	public function moveCellCategoryToTop( $categories ) {
	    $temp = array( 'Divi' => $categories['Divi'] );
	    unset( $categories['Divi'] );
	    return  $temp + $categories;
	}


	/**
	 * Add custom row modes elements to Layouts.
	 *
	 */
	protected function add_layout_row_types() {
		// Top Header
		$top = new WPDDL_Integration_Layouts_Row_Type_Top();
		$top->setup();

	    // Header
	    $header = new WPDDL_Integration_Layouts_Row_Type_Header();
	    $header->setup();

		// Content
		$content = new WPDDL_Integration_Layouts_Row_Type_Content();
		$content->setup();

		return $this;
	}


	/**
	 * This method can be used to remove all theme settings which are obsolete with the use of Layouts
	 * i.e. "Default Layout" in "Theme Settings"
	 *
	 */
	protected function modify_theme_settings() {
		// ...
	}
	
	public function modifyThemeWidgets() {

		unregister_sidebar( 'sidebar-2' );
		unregister_sidebar( 'sidebar-3' );
		unregister_sidebar( 'sidebar-4' );
		unregister_sidebar( 'sidebar-5' );

		return $this;
	}
}