<?php
/**
 * Integration loader. Determines if the integration should execute and if yes, execute it properly.
 *
 * When this file is loaded, we already know Layouts are active, theme integration support is loaded and it has
 * correct API version.
 *
 * See WPDDL_Theme_Integration_Abstract for details.
 *
 */
final class WPDDL_Divi_Integration extends WPDDL_Theme_Integration_Abstract {


	/**
	 * Theme-specific initialization.
	 *
	 * @return bool|WP_Error True when the integration was successful or a WP_Error with a sensible message
	 *     (which can be displayed to the user directly).
	 */
	protected function initialize() {

		// Setup the autoloader
		$autoloader = WPDDL_Theme_Integration_Autoloader::getInstance();
		$autoloader->addPaths( array(
			dirname( __FILE__ ) . '/application',
			dirname( __FILE__ ) . '/library/layouts/integration',
		) );

		// Run the integration setup
		$integration = WPDDL_Integration_Setup::get_instance();
		$result = $integration->run();

		return $result;
	}


	/**
	 * Determine whether the expected theme is active and the integration can begin.
	 *
	 * @return bool
	 */
	protected function is_theme_active() {
		return function_exists( 'et_setup_theme' );
	}


	protected function get_theme_name() {
		return "Divi";
	}

}

WPDDL_Divi_Integration::get_instance();