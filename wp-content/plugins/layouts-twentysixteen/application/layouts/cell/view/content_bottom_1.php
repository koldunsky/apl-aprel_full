<?php if ( is_active_sidebar( 'sidebar-2' ) ) : ?>
        <div class="widget-area">
                <?php dynamic_sidebar( 'sidebar-2' ); ?>
        </div><!-- .widget-area -->
<?php endif; ?>