<?php
/**
 * Header image cell
 */


/**
 * Cell abstraction. Defines the cell with Layouts.
 */
class WPDDL_Integration_Layouts_Cell_Header_Image extends WPDDL_Cell_Abstract {
	protected $id = '2016-heder-image';

	protected $factory = 'WPDDL_Integration_Layouts_Cell_Header_Image_Cell_Factory';
}


/**
 * Represents the actual cell.
 */
class WPDDL_Integration_Layouts_Cell_Header_Image_Cell extends WPDDL_Cell_Abstract_Cell {
	protected $id = '2016-heder-image';

	/**
	 * Each cell has it's view, which is a file that is included when the cell is being rendered.
	 *
	 * @return string Path to the cell view.
	 */
	protected function setViewFile() {
		return dirname( __FILE__ ) . '/view/header_image.php';
	}
}


/**
 * Cell factory.
 */
class WPDDL_Integration_Layouts_Cell_Header_Image_Cell_Factory extends WPDDL_Cell_Abstract_Cell_Factory {
	protected $name = 'Header Image';
	protected $description = 'This cell contains header image defined in theme settings (Appearance -> Customize). Recommended location for this cell is inside header row.';
	protected $allow_multiple = false;
	protected $cell_class = 'WPDDL_Integration_Layouts_Cell_Header_Image_Cell';

	public function __construct() {
       $this->preview_image_url = WPDDL_2016_URL_PUBLIC_IMG.'/website-header-image_expand-image.png';
    }

	public function get_editor_cell_template() {
		$this->setCategory();

		ob_start();
		?>
			<div class="cell-content">
			<p class="cell-name"><?php echo $this->category . ' - ' . $this->name; ?></p>
				<div class="cell-preview">
	                <div class="theme-integration-imagebox-area">
	                    <img src="<?php echo DDL_ICONS_SVG_REL_PATH . 'layouts-imagebox-cell.svg';?>">
					</div>
				</div>
			</div>
			</div>
		<?php
		return ob_get_clean();
	}

	protected function setCellImageUrl() {
		$this->cell_image_url = DDL_ICONS_SVG_REL_PATH . 'layouts-imagebox-cell.svg';
	}
}
