<?php
/**
 * SiteBranding cell
 */


/**
 * Cell abstraction. Defines the cell with Layouts.
 */
class WPDDL_Integration_Layouts_Cell_Site_Branding extends WPDDL_Cell_Abstract {
	protected $id = '2016-header-site-branding';

	protected $factory = 'WPDDL_Integration_Layouts_Cell_Site_Branding_Cell_Factory';
}


/**
 * Represents the actual cell.
 */
class WPDDL_Integration_Layouts_Cell_Site_Branding_Cell extends WPDDL_Cell_Abstract_Cell {
	protected $id = '2016-header-site-branding';

	/**
	 * Each cell has it's view, which is a file that is included when the cell is being rendered.
	 *
	 * @return string Path to the cell view.
	 */
	protected function setViewFile() {
		return dirname( __FILE__ ) . '/view/site_branding.php';
	}
}


/**
 * Cell factory.
 */
class WPDDL_Integration_Layouts_Cell_Site_Branding_Cell_Factory extends WPDDL_Cell_Abstract_Cell_Factory {
	protected $name = 'Site Title';
	protected $description = 'This is website branding cell and it contains website title and tagline. Recommended location for this cell is inside header row.';
	protected $allow_multiple = false;
	protected $cell_class = 'WPDDL_Integration_Layouts_Cell_Site_Branding_Cell';

    public function __construct() {
       $this->preview_image_url = WPDDL_2016_URL_PUBLIC_IMG.'/website-title-tagline_expand-image.png';
    }

	public function get_editor_cell_template() {
		$this->setCategory();

		ob_start();
		?>
			<div class="cell-content">
			<p class="cell-name"><?php echo $this->category . ' - ' . $this->name; ?></p>
				<div class="cell-preview">
	                <div class="theme-integration-sidebar-area">
	                    <img src="<?php echo plugins_url( '/../../../public/img/header-tagline.svg', __FILE__ ); ?>">
					</div>
				</div>
			</div>
			

		<?php
		return ob_get_clean();
	}

	protected function setCellImageUrl() {
		$this->cell_image_url = plugins_url( '/../../../public/img/header-tagline.svg', __FILE__ );
	}
}
