<?php

/**
 * Singleton for setting up the integration.
 *
 * Note that it doesn't have to have unique name. Because of autoloading, it will be loaded only once (when this
 * integration plugin is operational).
 *
 */
/** @noinspection PhpUndefinedClassInspection */
class WPDDL_Integration_Setup extends WPDDL_Theme_Integration_Setup_Abstract {

    
        private $row_count = 0;
    
	/**
	 * Run Integration.
	 *
	 * @return bool|WP_Error True when the integration was successful or a WP_Error with a sensible message
	 *     (which can be displayed to the user directly).
	 */
	public function run() {
            $this->addCustomRowModes();
	    $this->set_layouts_path( dirname( dirname(__FILE__) ) . DIRECTORY_SEPARATOR . 'public/layouts/' );
            return parent::run();
	}


	/**
	 * @todo Set supported theme version here.
	 * @return string
	 */
	protected function get_supported_theme_version() {
		return '1.0';
	}


	/**
	 * Build URL of an resource from path relative to plugin's root directory.
	 *
	 * @param string $relative_path Some path relative to the plugin's root directory.
	 * @return string URL of the given path.
	 */
	protected function get_plugins_url( $relative_path ) {
		return plugins_url( '/../' . $relative_path , __FILE__ );
	}


	/**
	 * Get list of templates supported by Layouts with this theme.
	 *
	 * @return array Associative array with template file names as keys and theme names as values.
	 */
	protected function get_supported_templates() {
		return array(
			'template-page.php' => __( 'Template page', 'ddl-layouts' )
		);
	}


	/**
	 * Layouts Support
	 *
	 * @todo Implement theme-specific logic here. For example, you may want to:
	 *     - if theme has it's own loop, replace it by the_ddlayout()
	 *     - remove headers, footer, sidebars, menus and such, if achievable by filters
	 *     - otherwise you will have to resort to something like redirecting templates (see the template router below)
	 *     - add $this->clear_content() to some filters to remove unwanted site structure elements
	 */
	protected function add_layouts_support() {

		parent::add_layouts_support();

		/** @noinspection PhpUndefinedClassInspection */
		WPDDL_Integration_Theme_Template_Router::get_instance();
                remove_action( 'twentysixteen_sidebars', 'twentysixteen_widgets_init' );
                add_action('ddl-row_start_callback', array($this, 'row_count'), 99, 2 );
                
                
	}
        
        public function row_count( $row, $target ){
            $this->row_count++;
            if( $this->row_count === 1 ){
		
                if($row->mode === 'twenty_sixteen_header'){
                    add_filter('ddl_render_row_end', array(&$this, 'append_html_to_row'), 98, 3);
                } else {
		    
                    add_filter('ddl_render_row_start', array(&$this, 'prepend_html_to_row'), 98, 2);
		    
                }
            }
        }
        
        
        
        public function prepend_html_to_row($html,$args){
	    if( $this->row_count === 1 ){
		ob_start();
		echo '<div id="content" class="site-content">'.$html;
		$html = ob_get_clean();
	    }
            return $html;
        }
        
        public function append_html_to_row($html,$args, $tag){
            
            if($args == 'twenty_sixteen_header'){
                ob_start();
                echo $html.'<div id="content" class="site-content">';
                $html = ob_get_clean();
            }
            return $html;
        }



	/**
	 * Add custom theme elements to Layouts.
	 *
	 */
	protected function add_layouts_cells() {

		$sidebar_cell = new WPDDL_Integration_Layouts_Cell_Sidebar();
		$sidebar_cell->setup();
                
                $content_bottom_cell_1 = new WPDDL_Integration_Layouts_Cell_Content_Bottom_1();
		$content_bottom_cell_1->setup();
                
                $content_bottom_cell_2 = new WPDDL_Integration_Layouts_Cell_Content_Bottom_2();
		$content_bottom_cell_2->setup();
                
                $header_image = new WPDDL_Integration_Layouts_Cell_Header_Image();
		$header_image->setup();
                
                $site_branding = new WPDDL_Integration_Layouts_Cell_Site_Branding();
		$site_branding->setup();
                
                $header_menu = new WPDDL_Integration_Layouts_Cell_Header_Menu();
		$header_menu->setup();
                
	}


	/**
	 * This method can be used to remove all theme settings which are obsolete with the use of Layouts
	 * i.e. "Default Layout" in "Theme Settings"
	 *
	 */
	protected function modify_theme_settings() {
		// ...
	}
        
        /**
	 * Add custom row modes.
	 */
	private function addCustomRowModes() {
		add_filter( 'ddl-get_rows_modes_gui_list', array($this, 'add_TwentySixteen_header_row_mode' ));
		add_filter('ddl_render_row_start', array($this, 'TwentySixteen_custom_row_open'), 98, 2);
		add_filter('ddl_render_row_end', array($this, 'TwentySixteen_custom_row_close'), 98, 3);
                
	}
        
        /**
	 * Header Row Mode
	 */
	public function add_TwentySixteen_header_row_mode($lists_html) {
		ob_start(); ?>
		<li>
                    <figure class="row-type">
                            <img class="item-preview" data-name="row_twenty_sixteen_header" src="<?php echo WPDDL_GUI_RELPATH; ?>dialogs/img/tn-boxed.png" alt="<?php _e('Twenty Sixteen header', 'ddl-layouts'); ?>">
                            <span><?php _e('Twenty Sixteen header row', 'ddl-layouts'); ?></span>
                    </figure>
                    <label class="radio" data-target="row_twenty_sixteen_header" for="row_twenty_sixteen_header" style="display:none">
                            <input type="radio" name="row_type" id="row_twenty_sixteen_header" value="twenty_sixteen_header">
                            <?php _e('Twenty Sixteen header', 'ddl-layouts'); ?>
                    </label>
		</li>

		<style type="text/css">
			.presets-list li{width:25%!important;}
		</style>
		<?php
		$lists_html .= ob_get_clean();

		return $lists_html;
	}

	public function TwentySixteen_custom_row_open($markup, $args) {

            if( $args['mode'] === 'twenty_sixteen_header' ){
                ob_start();?>
                <header id="masthead" class="site-header" role="banner">
			<div class="site-header-main">
                <?php
                $markup = ob_get_clean();
            }

            return $markup;
	}

	public function TwentySixteen_custom_row_close($output, $mode, $tag) {
            if( $mode === 'twenty_sixteen_header' ) {
                ob_start(); ?>
                </div><!-- .site-header-main -->
		</header><!-- .site-header -->
                
                <?php
                $output = ob_get_clean();
            }

            return $output;
	}


}